const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search AVG value of Term - 36 months and Mileage", ()=> {
    testpage.termMileage.click();
    testpage.forthDot.dragAndDrop(testpage.secondDot);
    testpage.saveBtn.click();
    browser.pause(2000);
    })

Then("results of related cars with AVG value of Term - 36 months are displayed", ()=>{
    if(testpage.secondDotResultText == '4782 to choose from'){
        console.log(`Result of AVG value of Term - 36 months serching is correct`)
    } else{
        console.log(`Consider an issue`);
     }  
})
