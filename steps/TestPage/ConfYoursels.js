const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search for 'Configure yourself'", ()=> {
    testpage.PopularfiltersBtn.click();
    testpage.confYourselfBtn.click();
    testpage.saveBtn.click();
    browser.pause(2000);
    })

Then("results of related cars sums are displayed", ()=> {
    if(testpage.confYouselfSumText == '4764 to choose from'){
        console.log(`Result of a 'Configure yourself' filter is correct`)
    }else{
       console.log(`Consider an issue`);
    }                    
})